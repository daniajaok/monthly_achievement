const browseFile = document.getElementById('browse_file')
const fileUpload = document.getElementById('file_upload')
const fileName = fileUpload.querySelector('.file__name')
const buttonRemoveFile = document.getElementById('file_remove')

browseFile.addEventListener('change', (e) => {
    const file = e.target.files[0]
    var sizeFileKb = file.size / 1000
    var nameFile = file.name
    if (nameFile.length > 30) {
        var exte = nameFile.split('.')[1]
        var name = nameFile.split('.')[0]
        name = name.slice(0, 25) + '...' + name.substr(name.length - 2)
        nameFile = name + '.' + exte
    }
    fileUpload.style.display = 'flex'
    fileName.textContent = `${sizeFileKb} Kb ${nameFile}`
})

buttonRemoveFile.addEventListener('click', (e) => {
    fileUpload.style.display = 'none'
})