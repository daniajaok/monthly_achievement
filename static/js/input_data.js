const choices = document.querySelectorAll('select.input__data')
const inputs = document.querySelectorAll('input.input__data')

const changeAllFalse = () => {
    choices.forEach(select => {
        select.value = 0
    })
} 

choices.forEach(select => {
    select.addEventListener('change', (e) =>{
        if (parseInt(e.target.value) === 1) {
            changeAllFalse()
            select.value = 1
        }
    })
})

inputs.forEach(input => {
    input.addEventListener('input', (e) => {
        var val = e.target.value
        val = val.replace(/[^0-9\.]/g, '')
        input.value = val
    })
})