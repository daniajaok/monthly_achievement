from django.shortcuts import render, redirect
from django.conf import settings
from django.template.defaulttags import register
import pandas as pd
import numpy as np
from sklearn import feature_selection as fs
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from scipy import stats
import json

# Custom filter dibuat ketika kita ingin menggunakan filter template yang tidak ada dibawaan
# Menambahkan custom filter yang akan dipakai di template (silahkan lihat file evaluation.html)
# Filter ini akan menghasilkan type data dari parameter. Contoh: str, dict, int
@register.filter 
def get_type(value):
    return type(value).__name__ 

# Menambahkan custom filter yang akan dipakai di template (silahkan lihat file evaluation.html)
# Filter ini akan menghasilkan string yang sudah di format. Contoh: 10.63263723 => '10.63'
@register.filter
def format_number(value): 
    return '{:,.2f}'.format(value)


BASE_DIR = settings.BASE_DIR

# Data default yang akan digunakan di prediction result
# Data terisi saat user menginput nilai di halaman Input Data
# Supaya data dapat dirubah di dalam function view dan dipakai di function view yang lain 
# Data harus terdefinisi sebagai dict di luar function view
DATA_WEB = {
    'tahun': 0,
    'bulan': 0,
    'trg_bln': 0,
    'bln': 0,
    'AIR_TANAH': 0,
    'BPHTB': 0,
    'HIBURAN': 0,
    'HOTEL': 0,
    'MINERAL': 0,
    'PARKIR': 0,
    'PBB': 0,
    'PENERANGAN': 0,
    'REKLAME': 0,
    'RESTORAN': 0,
}

data_before_rasformasi = pd.read_excel(BASE_DIR / 'static/datasets/sebelum_di_tasformasi.xlsx')
data_after_rasformasi = pd.read_excel(BASE_DIR / 'static/datasets/rasformasi_v3.xlsx')

x = data_after_rasformasi.loc[:, [
    'Tahun', 'Bulan','Target_Bulannan','Bulan_Ini','AIR TANAH','BPHTB','HIBURAN','HOTEL',
    'MINERAL BUKAN LOGAM & BATUAN','PARKIR','PBB','PENERANGAN JALAN','REKLAME', 'RESTORAN'
]]
y = data_after_rasformasi.loc[:, ['label']]
parameter_V1 = {
    'penalty': 'l2',
    'dual': False,
    'tol': 0.0001,
    'C': 1.0,
    'fit_intercept': True,
    'intercept_scaling': 1,
    'class_weight': None,
    'random_state': 25,
    'solver': 'lbfgs',
    'max_iter': 1000,
    'multi_class': 'auto',
    'verbose': 0
}
model_Lr = LogisticRegression(**parameter_V1)
rfe = fs.RFE(model_Lr)
y = y.to_numpy().ravel()
rfe.fit(x,y)

x1 = data_after_rasformasi.loc[:, [
    'Target_Bulannan','Bulan_Ini','HIBURAN','MINERAL BUKAN LOGAM & BATUAN',
    'PARKIR','PARKIR','PENERANGAN JALAN','REKLAME'
]]
x2 = data_after_rasformasi.loc[:, [
    'Target_Bulannan','Bulan_Ini','AIR TANAH','BPHTB','HIBURAN','HOTEL',
    'MINERAL BUKAN LOGAM & BATUAN','PARKIR','PARKIR','PBB','PENERANGAN JALAN','REKLAME',
    'RESTORAN','RESTORAN'
]]

# ini yangtidak mengunakan varibale dipilih mengunakan feature_selection 
X_train, X_test, y_train, y_test = train_test_split(
    x,y,test_size = 0.2,random_state=30,train_size=None, shuffle=True,stratify=None
)
model_Lr.fit(X_train,y_train)
y_pediksi = model_Lr.predict(X_test)
ConfusionMatrix = confusion_matrix(y_test, y_pediksi)
ClassificationReport = classification_report(y_test, y_pediksi, output_dict=True)


def fake_input(request):
    context = {
        'title': 'Input File'
    }
    return render(request, 'fake_input.html', context=context)


def before_rasformasi(request):
    title_data = data_before_rasformasi.columns
    json_records = data_before_rasformasi.reset_index().to_json(orient='records')
    data_csv = json.loads(json_records)
    context = {
        'title_page': 'Before Rasformasi',
        'title_data': title_data,
        'content_data': data_csv,
    }
    return render(request, 'rasformasi.html', context=context)
    

def after_rasformasi(request):
    title_data = data_after_rasformasi.columns
    json_records = data_after_rasformasi.reset_index().to_json(orient='records')
    data_csv = json.loads(json_records)
    context = {
        'title_page': 'After Rasformasi',
        'title_data': title_data,
        'content_data': data_csv,
    }
    return render(request, 'rasformasi.html', context=context)


def evaluation_result(request):
    context = {
        'title_page': 'Evaluation Result',
        'confusion_matrix': ConfusionMatrix,
        'classification_report': ClassificationReport,
    }
    return render(request, 'evaluation_result.html', context=context)


def input_data(request):
    if request.method == 'POST':
        DATA_WEB['tahun'] = int(request.POST.get('tahun') or 0)
        DATA_WEB['bulan'] = int(request.POST.get('bulan') or 0)
        DATA_WEB['trg_bln'] = int(request.POST.get('trg_bln') or 0)
        DATA_WEB['bln'] = int(request.POST.get('bln') or 0)
        DATA_WEB['AIR_TANAH'] = int(request.POST.get('air') or 0)
        DATA_WEB['BPHTB'] = int(request.POST.get('bphtb') or 0)
        DATA_WEB['HIBURAN'] = int(request.POST.get('hiburan') or 0)
        DATA_WEB['HOTEL'] = int(request.POST.get('hotel') or 0)
        DATA_WEB['MINERAL'] = int(request.POST.get('mineral') or 0)
        DATA_WEB['PARKIR'] = int(request.POST.get('parkir') or 0)
        DATA_WEB['PBB'] = int(request.POST.get('pbb') or 0)
        DATA_WEB['PENERANGAN'] = int(request.POST.get('penerangan') or 0)
        DATA_WEB['REKLAME'] = int(request.POST.get('reklame') or 0)
        DATA_WEB['RESTORAN'] = int(request.POST.get('restoran') or 0)
        return redirect('prediction_result')
    context = {
        'title_page': 'Input Data',
    }
    return render(request, 'input_data.html', context=context)


def prediction_result(request):
    Data_uji = pd.read_excel(BASE_DIR / 'static/datasets/Input_Data_Uji.xlsx')
    def zscore(Data_uji):
        Data_uji["Bulan_Ini"] = np.abs(stats.zscore(Data_uji['Bulan_Ini']))
        Data_uji["Target_Bulannan"] = np.abs(stats.zscore(Data_uji['Target_Bulannan']))

    def Gabungan(Data_uji):
        for x in range(0,2):
            Data_uji["Target_Bulannan"] =np.sqrt(Data_uji["Target_Bulannan"])
            Data_uji["Bulan_Ini"] =np.sqrt(Data_uji["Bulan_Ini"])

    def masukandata(row):
        Data_uji.loc[len(Data_uji)] = row
        zscore(Data_uji)
        Gabungan(Data_uji)

    def ubah_nilai(hasil):
        if hasil == 1:
            return 'Tercapai'
        else:
            return 'Tidak Tercapai'

    new_data = {
        'Tahun': DATA_WEB['tahun'], 
        'Bulan': DATA_WEB['bulan'], 
        'Target_Bulannan': DATA_WEB['trg_bln'],
        'Bulan_Ini': DATA_WEB['bln'],
        'AIR TANAH': DATA_WEB['AIR_TANAH'],
        'BPHTB': DATA_WEB['BPHTB'],
        'HIBURAN': DATA_WEB['HIBURAN'],
        'HOTEL': DATA_WEB['HOTEL'],
        'MINERAL BUKAN LOGAM & BATUAN': DATA_WEB['MINERAL'],
        'PARKIR': DATA_WEB['PARKIR'],
        'PBB': DATA_WEB['PBB'],
        'PENERANGAN JALAN': DATA_WEB['PENERANGAN'],
        'REKLAME': DATA_WEB['REKLAME'],
        'RESTORAN': DATA_WEB['RESTORAN'],
    }
    new_data_tampil = {
        'Tahun': [DATA_WEB['tahun']],
        'Bulan': [DATA_WEB['bulan']],
        'Target_Bulannan': [DATA_WEB['trg_bln']],
        'Bulan_Ini': [DATA_WEB['bln']],
        'AIR TANAH': [DATA_WEB['AIR_TANAH']],
        'BPHTB': [DATA_WEB['BPHTB']],
        'HIBURAN': [DATA_WEB['HIBURAN']],
        'HOTEL': [DATA_WEB['HOTEL']],
        'MINERAL BUKAN LOGAM & BATUAN': [DATA_WEB['MINERAL']],
        'PARKIR': [DATA_WEB['PARKIR']],
        'PBB': [DATA_WEB['PBB']],
        'PENERANGAN JALAN': [DATA_WEB['PENERANGAN']],
        'REKLAME': [DATA_WEB['REKLAME']],
        'RESTORAN': [DATA_WEB['RESTORAN']],
    }
    data_tampilan = pd.DataFrame(new_data_tampil)
    masukandata(new_data)

    y_predict=model_Lr.predict(Data_uji)
    Data_uji['label'] = y_predict
    Data_uji[[
        'Tahun', 'Bulan','Target_Bulannan','Bulan_Ini','AIR TANAH','BPHTB','HIBURAN','HOTEL',
        'MINERAL BUKAN LOGAM & BATUAN','PARKIR','PARKIR','PBB','PENERANGAN JALAN','REKLAME',
        'RESTORAN','RESTORAN','label'
    ]]
    Data_uji=Data_uji.tail(1)

    hasil_predic=Data_uji['label'].tail(1).values[0]
    hasil_predic1 = pd.DataFrame({'label': [hasil_predic]})
    data_tampilan['Hasil']=pd.concat([hasil_predic1], axis=1)
    data_tampilan['Hasil'] = data_tampilan['Hasil'].apply(ubah_nilai)

    title_data = data_tampilan.columns
    json_records = data_tampilan.reset_index().to_json(orient='records')
    data_csv = json.loads(json_records)
    context = {
        'title_page': 'Prediction Result',
        'title_data': title_data,
        'content_data': data_csv,
    }
    return render(request, 'rasformasi.html', context=context)