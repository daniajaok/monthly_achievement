from django.contrib import admin
from django.urls import path
from achievement import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.fake_input, name='fake_input'),
    path('before-rasformasi', views.before_rasformasi, name='before_rasformasi'),
    path('after-rasformasi', views.after_rasformasi, name='after_rasformasi'),
    path('evaluation-result', views.evaluation_result, name='evaluation_result'),
    path('input-data', views.input_data, name='input_data'),
    path('prediction-result', views.prediction_result, name='prediction_result'),
]
